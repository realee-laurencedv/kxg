# Kicad Xyrs Generator

Simple python xyrs generator from bom.csv and position.csv files (generic export in both KiCAD stable 5.1 and nightly 5.99)  

XYRS format is following MacroFab [template][mfxyrs_link] and currently part size are all fixed at 1x1mm square


[mfxyrs_link]:  https://macrofab.com/knowledgebase/required-design-files/
