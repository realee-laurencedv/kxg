#!/usr/bin/python3

import csv
import argparse
import sys


""" data container for a single instance of part in design """


class parts(object):

    designator =    None
    value =         None
    MPN =           None
    populate =      None
    posX =          None
    posY =          None
    lenX =          None
    lenY =          None
    orientation =   None
    PCBside =       None
    mountType =     None
    footprint =     None

    """docstring for parts"""
    def __init__(self):
        super(parts, self).__init__()


""" KiCAD XYRS Generator """


class kxg(object):

    schcsvfile = None
    pcbcsvfile = None
    destfile = None

    partsList = []

    mm2mils_ratio = 39.3700787

    """docstring for kxg"""
    def __init__(self, arg):
        super(kxg, self).__init__()
        """ Arguments """
        argParse = argparse.ArgumentParser()
        argParse.add_argument("-b", "--bom", help="BOM csv file", type=str, required=True)
        argParse.add_argument("-p", "--pos", help="Position csv file", type=str, required=True)
        argParse.add_argument("-f", "--format", help="input format units system", type=str, choices=["metric", "imperial"], default="imperial")
        argParse.add_argument("-o", "--out", help="Output XYRS file", type=str, default="out.xyrs")
        args = argParse.parse_args()

        self.schcsvfile = args.bom
        self.pcbcsvfile = args.pos
        self.destfile = args.out
        self.format = args.format

    def run(self):
        """ read input files """
        self.readSCHcsv(self.schcsvfile)
        self.readPCBcsv(self.pcbcsvfile)

        """ convert unit to output req (fuck the british empire) """
        if (self.format == 'metric'):
            print("metric to inch conversion requested")
            self.convertUnit(posRatio=self.mm2mils_ratio, lenRatio=self.mm2mils_ratio)
        """ then convert to mils instead of inch """
        self.convertUnit(posRatio=1000)
        self.mirrorPosOnBottom()

        """ verify that every mandatory value has at least a sane one """
        self.sanitize()

        """ write output file """
        self.writeXYRS(self.destfile)

    def readSCHcsv(self, src=None):
        if src is not None:
            with open(self.schcsvfile, 'r') as csvfile:
                csvReader = csv.reader(csvfile, delimiter=',')
                csvContent = list(csvReader)
                """ Assumption: first line is map """
                """ Find which col contain the important data """
                designatorColID = csvContent[0].index('Reference')
                valColID = csvContent[0].index(' Value')
                MPNColID = csvContent[0].index(' MPN')
                DNPColID = csvContent[0].index(' DNP')

                """ Assumption: all named col have been found """

                """ Extract data from source and put it in ram object """
                for row in csvContent[1:]:
                    newPart = parts()
                    newPart.designator = row[designatorColID]
                    newPart.value = row[valColID]
                    newPart.MPN = row[MPNColID]
                    if row[DNPColID] == '1':
                        newPart.populate = 0
                    else:
                        newPart.populate = 1
                    """ Force a valid value to unknown data (not avail. in input files) """
                    newPart.mountType = 'SMD'       # force all part to default to SMD
                    newPart.lenX = 100.0              # add a part size so we have something to output to file
                    newPart.lenY = 100.0
                    self.partsList.append(newPart)
                print("Found {} parts".format(len(self.partsList)))
            print('Schematic CSV file read')

    def readPCBcsv(self, src=None):
        if src is not None:
            with open(self.pcbcsvfile, 'r') as csvfile:
                csvReader = csv.reader(csvfile, delimiter=',')
                csvContent = list(csvReader)
            """ Assumption: first line is map """
            """ Find which col contain the important data """
            designatorColID = csvContent[0].index('Ref')
            footprintColID = csvContent[0].index('Package')
            posXColID = csvContent[0].index('PosX')
            posYColID = csvContent[0].index('PosY')
            orientationColID = csvContent[0].index('Rot')
            PCBsideColID = csvContent[0].index('Side')

            """ Extract data from source and put it in ram object """
            for row in csvContent[1:]:
                """ find correct part and add the information """
                foundPart = next((x for x in self.partsList if x.designator == row[designatorColID]), None)
                foundPart.footprint = str(row[footprintColID])
                foundPart.posX = float(row[posXColID])
                foundPart.posY = float(row[posYColID])
                foundPart.orientation = float(row[orientationColID])
                foundPart.PCBside = str(row[PCBsideColID])

            print('PCB CSV file read')

    def sanitize(self):
        if len(self.partsList) > 0:
            for part in self.partsList:
                if part.designator is None:
                    part.designator = 'None'
                if part.value is None:
                    part.value = 'None'
                if part.MPN is None:
                    part.MPN = 'None'
                if part.populate is None:
                    part.populate = 'None'
                if part.posX is None:
                    part.posX = 0.0
                if part.posY is None:
                    part.posY = 0.0
                if part.lenX is None:
                    part.lenX = 0.0
                if part.lenY is None:
                    part.lenY = 0.0
                if part.orientation is None:
                    part.orientation = 0.0
                if part.PCBside is None:
                    part.PCBside = 'None'
                if part.mountType is None:
                    part.mountType = 'None'
                if part.footprint is None:
                    part.footprint = 'None'

    def mirrorPosOnBottom(self):
        if len(self.partsList) > 0:
            for part in self.partsList:
                if part.PCBside == 'bottom':
                    print('Flipped {} on X axis'.format(part.designator))
                    part.posX *= -1

    def writeXYRS(self, dest=None):
        if dest is not None:

            with open(dest, 'w+') as destFile:
                csvWriter = csv.writer(destFile, delimiter='\t', quoting=csv.QUOTE_MINIMAL)
                csvWriter.writerow('#Unit is mils'.split('\t'))
                csvWriter.writerow('#Designator    X-Loc   Y-Loc   Rotation    Side    Type    X-Size  Y-Size  Value   Footprint   Populate    MPN'.split('\t'))

                for parts in self.partsList:
                    print('{}\t| X {:1.6}\t| Y {:1.6}\t| {}'.format(parts.designator, parts.posX, parts.posY, parts.PCBside))
                    csvWriter.writerow([parts.designator, '{:1.6}'.format(parts.posX), '{:1.6}'.format(parts.posY),
                                        '{:1.6}'.format(parts.orientation), parts.PCBside, parts.mountType,
                                        '{:1.6}'.format(parts.lenX), '{:1.6}'.format(parts.lenY), parts.value, parts.footprint,
                                        parts.populate, parts.MPN])

            print('Output file written')

    def convertUnit(self, lenRatio=None, posRatio=None):
        for part in self.partsList:
            if posRatio is not None:
                if part.posX is not None:
                    part.posX *= posRatio
                if part.posY is not None:
                    part.posY *= posRatio
            if lenRatio is not None:
                if part.lenX is not None:
                    part.lenX *= lenRatio
                if part.lenY is not None:
                    part.lenY *= lenRatio


if __name__ == '__main__':
    obj = kxg(sys.argv)
    obj.run()
    sys.exit(0)
